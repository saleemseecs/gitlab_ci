var mysql = require('mysql');

module.exports = {
 createDbConnection: function(){ 
    var con= mysql.createConnection({
        host: "localhost",
        //host: "127.0.0.1",
        user: "root",
        password: "",
        database: "testCaseResults"
    });
    return con;
  }
};
